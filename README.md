# PhD-Report

This is the repo of my journey throw my PhD. It will contain my report, and my journal.

The subject is [here](https://notes.inria.fr/3YWkVP1aR6eBfUPLNsDsWA#).
The linked notes are [here](https://notes.inria.fr/N0XieIqORPOP9vENmuxZgA#).


## draw io
use `hediet.vscode-drawio` xtension

## Zotero integration in obsidian
The zotero integration needs the [zotserver](https://github.com/MunGell/ZotServer) plugin