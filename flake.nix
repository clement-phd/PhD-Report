{
  description = "Global file database";

  inputs = {
    nixpkgs.url = "github:NixOS/nixpkgs/nixos-23.11";
    flake-utils.url = "github:numtide/flake-utils";
  };

  outputs = { self, nixpkgs, flake-utils, ... }@inputs:
    flake-utils.lib.eachDefaultSystem (system:
      let
        pkgs = nixpkgs.legacyPackages.${system};
        pythonPackages = pkgs.python310Packages;
      in
      {
        

        devShell = pkgs.mkShell rec {
          venvDir = "./.venv";
          buildInputs = with pkgs; [
            # python 
            pythonPackages.python
            pythonPackages.venvShellHook
            autoPatchelfHook

            # other
            zip
            unzip

          ];

        
          postVenvCreation = ''
            unset SOURCE_DATE_EPOCH
            pip install -r requirements.txt
            autoPatchelf ./venv
          '';

          postShellHook = ''
            unset SOURCE_DATE_EPOCH
          '';

          
        };
      }
    );
}
