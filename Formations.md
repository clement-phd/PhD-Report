#formation 
List of all the formations [here](https://ed-matisse.doctorat-bretagne.fr/fr/formations#p-142)

Need 100 hours in total
40 transversale
40 scientifique

[entrepreunariat](https://ed-matisse.doctorat-bretagne.fr/sites/ed-matisse.doctorat-bretagne.fr/files/medias/files/CDB-FormationsEntrepreneuriat_v4.pdf) (compte comme transversale, necessite 20h) -> [amethist](https://amethis.doctorat-bretagneloire.fr/amethis-client/formation/gestion/formation/2272), meilleur : ENTPR-CDB-09 (attendre deuxieme semestre -> decembre/janvier ?)

besoin aussi d'une formation à l'ethique (compte comme transversale, necessite 5h ?) -> [amethist]([https://amethis.doctorat-bretagneloire.fr/amethis-client/formation/gestion/formation/2853](https://amethis.doctorat-bretagneloire.fr/amethis-client/formation/gestion/formation/2853 "https://amethis.doctorat-bretagneloire.fr/amethis-client/formation/gestion/formation/2853")) 
## Equivalences
The array of equivalence [here](https://ed-matisse.doctorat-bretagne.fr/sites/ed-matisse.doctorat-bretagne.fr/files/medias/files/tableau%20equivalences_0.pdf)

- 5 hours (scientifique) of PhD defense : Quentin and Gwendal (2 done)
- 32h (transversale) mission d'enseignement : 54h au deuxieme semestre (valider) 
	- plateforme cours : [here](https://ose.univ-rennes1.fr/).
## Scientifiques
Le plus interessant = summer school ~ 25 heures
Sinon : formation CIF : [here](http://master.irisa.fr/courses/lst_courses_rennes.html) . 