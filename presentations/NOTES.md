# This is notes for the presentations


## CVE example :

 > difficult to modelize (adding to fix the issue is hard)
- [CVE-2022-48468](https://cve.mitre.org/cgi-bin/cvename.cgi?name=CVE-2022-48468) : 
infected : 
```c
subm = protobuf_c_message_unpack(scanned_member->field->descriptor,
    allocator,
    len - pref_len,
    data + pref_len);
```

fixed : 
```c
if (len >= pref_len)
    subm = protobuf_c_message_unpack(scanned_member->field->descriptor,
        allocator,
        len - pref_len,
        data + pref_len);
```