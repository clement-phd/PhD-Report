# use [slidev](https://sli.dev/guide/install)

To launch the presentation, use the following command:

```bash
npx slidev
```

## build the pdf

> WARN : the node package ( `playwright-chromium` and `playwright-core`) and the flake package (`playwright-driver.browsers`) of playwright must be of the same version.

```bash
npx slidev export --with-clicks --with-toc

```