# Tasks (roadmap)

- **Task 1**: test and select tree-sitter queries to build a database of models of vulnerabilities
- **Task 2**: efficient download of heritage software source code
- **task 3**: Apply the queries to the source code

---
src : ./tasks/task1.md
---

---
src : ./tasks/task2.md
---

---
src : ./tasks/task3.md
---