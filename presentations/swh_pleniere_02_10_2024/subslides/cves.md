# CVEs : Common Vulnerabilities and Exposures

<v-switch unmount=true>
<template #0>

## Definition

- Known security vulnerabilities with a unique identifier (for example CVE-2021-12345)
- Today : 240,830 CVEs in the mitre database


## Database

- MITRE : founded in 1999 with the paper `"Towards a Common Enumeration of Vulnerabilities"` by David E. Mann and Steven M. Christey
- NVD : National Vulnerability Database > U.S. government repository
- These databases aren't properly structured !!!
- Difficulty to find the code source associated with the CVE
- OSV : Google's Open Source Vulnerabilities database, partially solve the problem
</template>

<template #1>

## Crawling

- Goal : Collecting the most CVEs possible with their associated source code
- use 4 sources :
    - Open CVE : semi structured data in PostgreSQL
    - Ground truth : CSV file from the paper `“A Ground-Truth Dataset of Real Security Patches”` by Sofia Reis and Rui Abreu
    - CVEFixes : sqlite database from the paper `“CVEfixes: Automated Collection of Vulnerabilities and Their Fixes from Open-Source Software”` by Guru Bhandari, Amara Naseer, and Leon Moonen
    - Complete using the OSV database

<strong> 28 261 CVEs with an useable url of fix</strong>
</template>
</v-switch>

Jafar Akhoundali et al., “MoreFixes: A Large-Scale Dataset of CVE Fix Commits Mined through Enhanced Repository Discovery,” in Proceedings of the 20th International Conference on Predictive Models and Data Analytics in Software Engineering (PROMISE ’24: 20th International Conference on Predictive Models and Data Analytics in Software Engineering, Porto de Galinhas Brazil: ACM, 2024), 42–51, https://doi.org/10.1145/3663533.3664036.