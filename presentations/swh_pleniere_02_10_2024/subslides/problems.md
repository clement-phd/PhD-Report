---
hideInToc: true
---

<div class="absolute top-10 right-4">
CVE-2022-48468
</div>

# Problems

- There are thousands vulnerability discovers each years, and this not possible to know if there are fixes or not still in the version you use
- The software always evolve and this is difficult to ensure the safety of the library or tools you use
- A lot of vulnerabilities are actually similar and are common developer mistakes which can be avoided with proper tools

<v-switch unmount=true>
<template #0>

## Infected

```c
subm = protobuf_c_message_unpack(scanned_member->field->descriptor,
    allocator,
    len - pref_len,
    data + pref_len);
```

</template>
<template #1>

## Fixed

```c
if (len >= pref_len)
    subm = protobuf_c_message_unpack(scanned_member->field->descriptor,
        allocator,
        len - pref_len,
        data + pref_len);
```


</template>
</v-switch>

<strong v-click="2"> How to help developers improve the security of their software using open source knowledge ? </strong>
