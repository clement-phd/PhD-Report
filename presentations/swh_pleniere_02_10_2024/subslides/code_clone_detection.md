# Code clones (of CVEs) detection

> Qurat Ul Ain et al., `A Systematic Review on Code Clone Detection,` IEEE Access 7 (2019): 86121–44, https://doi.org/10.1109/ACCESS.2019.2918202.

<v-switch unmount=true>
<template #0>

## Type of code clones

- **Exact clones (Type 1)**: Identical code segments except for changes in comments, layouts and whitespaces.
- **Renamed clones (Type 2)**: Code segments which are syntactically or structurally similar other than changes in comments, identifiers, types, literals, and layouts. These clones are also called parameterized clones.
- **Near Miss clones (Type 3)**: Copied pieces with further modification such as addition or removal of statements and changes in whitespaces, identifiers, layouts, comments, and types but outcomes are similar. These clones are also known as gapped clones.
- **Semantic clones (Type 4)**: More than one code segments that are functionally similar but implemented by different syntactic variants



</template>

<template #1>

## Type of detection

- **Textual approaches**: It detects type 1 clones more effectively, but also type 2 and type 3 clones.
- **Lexical approaches**: Lexical approaches are also known as token-based approaches and able to identify type 2 clones efficiently, but also type 1 and 3 clones.
- **Tree-based approaches**: They are most effective for the detection of type 3 clones, but can also detect type 1, 2, and 4 clones.
- **Metric based approaches**: They can detect type 3 clones effectively, but also type 1, 2 and 4 clones.
- **Semantic approaches**: Use execution or PDG. It have the ability to detect type 1, 2, 3 and mainly 4 clones.
- **Hybrid approaches**: It is a combination of two or more techniques e.g. (Textual, Lexical, Syntactic or Semantic)


</template>
</v-switch>