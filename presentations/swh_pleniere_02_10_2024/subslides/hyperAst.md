# Scaling problem and temporal representation : HyperAst

> Quentin Le Dilavrec et al., `HyperAST: Enabling Efficient Analysis of Software Histories at Scale,` in Proceedings of the 37th IEEE/ACM International Conference on Automated Software Engineering, ASE ’22 (New York, NY, USA: Association for Computing Machinery, 2023), 1–12, https://doi.org/10.1145/3551349.3560423.

- Load AST for multiple versions of a code base at once
- Remove duplicates
- Create an efficient representation of the code base

The objective is to apply the tree-sitter queries to this representation to speed up the detection of vulnerabilities.