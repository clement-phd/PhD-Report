
## Task 1 : test and select tree-sitter queries to build a database of models of vulnerabilities

### 1. Download the CVEs source code and fix

- Crawl the git repository and isolate the modified files of the commit associated with the fix of the CVE

<img src="/experiments/build_cve_database.drawio.svg" class="h-85" />

---
hideInToc: true
---

## Task 1 : test and select tree-sitter queries to build a database of models of vulnerabilities

### 2. Problems and solutions of the tree-sitter queries

<div v-click>

- How to transform a fix into a query of the vulnerability ?

</div>

<div v-click>

> Take the code before the fix

</div>

<div v-click>

- How to capture the vulnerability when the fix only consist to add code ?

</div>

<div v-click>

> Take a context

</div>

<div v-click>

- How to determine the context ?

</div>

<div v-click>

>2 solutions :
>- Take a range of code (But this can lead to corrupted AST)
>- Take the parent node of the fix (But this can be too large)

</div>

<div v-click>

- How to handle the case of multiple places of the fix ?

</div>

<div v-click>

> Make a query for each place

</div>

---
hideInToc: true
---

## Task 1 : test and select tree-sitter queries to build a database of models of vulnerabilities

### 3. Test the queries

- Must eliminate the queries that are not relevant (too many false positives or/and too many false negatives)

> A good dataset which can be used, is the cve's files which can be labeled as vulnerable or not

- Eliminate the queries that never match (under x%)
- Eliminate the queries that match too much (over y%)
- Eliminate CVEs that have too much queries eliminated