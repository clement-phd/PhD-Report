
## Task 3 : Apply the queries to the source code

- Load each queries into hyperAST
- For each repository retrieved by the task 2 :
    - Load it into hyperAST
    - Test all queries on it

> Use a limited dataset first (For example a C subset), then calculate the time needed to process the whole dataset