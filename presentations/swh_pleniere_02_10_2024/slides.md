---
theme: default
layout: cover
author: Lise LAHOCHE

hideInToc: true
---

<div class="grid grid-cols-2 gap-4 h-20">
<img src="/inria_logo.png" class="h-20" />

<img src="/software_heritage_logo.jpg" class="h-20" />
</div>

<img src="/DiverSE_logo.png" class="absolute bottom-0 right-4 h-35" />

# Modeling, classification and detection of vulnerabilities and their variants in software code bases via Software Heritage

{{$slidev.configs.author}}, INRIA, swhsec
<br>tutor : Olivier BARAIS and Olivier ZENDRA

<style>
h1 {
  font-size: 40px;
}
</style>

---
src: ./subslides/problems.md
---

---
hideInToc: true
---

# Table of Contents

<Toc maxDepth="2"/>

---

# Goals

<v-clicks>

- Build a database of models of vulnerabilities reliable and up-to-date
- Develop a method to detect vulnerabilities in code bases
- Make this method quick and efficient
- Use the immuability of the Software Heritage archive to label already known and new infected code
- Propose an easy to use tool

</v-clicks>

---
src: ./subslides/cves.md
---

---
src: ./subslides/code_representations.md

<!-- These representation can help us to detect code clone, and in particular CVEs one -->
---

---
src: ./subslides/code_clone_detection.md
---

---

# Software Heritage

- Preserve open source software 
<img src="/swh/swh-stats-05-24.png"/>
- 4 pb of data
- Regular crawling : Expending the archive

---
src: ./subslides/tree_sitter.md
---

---
src: ./subslides/hyperAst.md
---

---
src: ./subslides/tasks.md
---