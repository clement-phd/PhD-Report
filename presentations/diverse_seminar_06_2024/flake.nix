{
  description = "dev env";

  inputs = {
    nixpkgs.url = "github:NixOS/nixpkgs/release-24.05";
  };

  outputs = { self, nixpkgs }: let
    system = "x86_64-linux";
    pkgs = import nixpkgs { inherit system; };
  in {
      devShells.${system}.default = pkgs.mkShell {
      packages = with pkgs; [
        nodejs
        playwright-driver.browsers # 1.40.0

        graphviz
      ];

      
      #shellHook = "";
      shellHook = ''
        export PLAYWRIGHT_NODEJS_PATH=${pkgs.nodejs}/bin/node
        export PLAYWRIGHT_BROWSERS_PATH=${pkgs.playwright-driver.browsers}
        export PLAYWRIGHT_SKIP_VALIDATE_HOST_REQUIREMENTS=true
        export PLAYWRIGHT_SKIP_BROWSER_DOWNLOAD=true
      '';
    };
  };
}
