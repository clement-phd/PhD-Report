
# Code Representation, AST and Graphs

<div class="absolute top-25 right-4">
CVE-2022-48468
</div>

<v-switch unmount=true>
<template #0>

## Textual representation

### Infected

```c
subm = protobuf_c_message_unpack(scanned_member->field->descriptor,
    allocator,
    len - pref_len,
    data + pref_len);
```

### Fixed

```c
if (len >= pref_len)
    subm = protobuf_c_message_unpack(scanned_member->field->descriptor,
        allocator,
        len - pref_len,
        data + pref_len);
```

</template>
<template #1>

## To abstract syntax tree (AST)
<img src="/cve-2022-48468/ast/fixed.drawio.svg" class="h-95" />

</template>
<template #2>

## To abstract syntax tree (AST)
## Or to Program Dependence Graph (PDG)

<img src="/cve-2022-48468/pdg/fixed.drawio.svg" class="" />

</template>
<template #3>

## To abstract syntax tree (AST)
## Or to Program Dependence Graph (PDG)
## Or to Control Flow Graph (CFG)

<img src="/cve-2022-48468/cfg/fixed.drawio.svg" class="" />

</template>
<template #4>

## To global graph


</template>
<template #5>

## To global graph

<img src="/cve-2022-48468/global_graph/fixed1.drawio.svg" class="h-90" />

</template>
<template #6>

## To global graph

<img src="/cve-2022-48468/global_graph/fixed2.drawio.svg" class="h-90" />

</template>
<template #7>

## To global graph

<img src="/cve-2022-48468/global_graph/fixed3.drawio.svg" class="h-90" />

</template>
</v-switch>