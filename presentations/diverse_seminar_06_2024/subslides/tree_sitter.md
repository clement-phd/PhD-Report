# <img src="/tree-sitter-small.png" class="inline h-10"/> Tree-sitter and his query language as a modelization tool and detection tool

<v-switch unmount=true>
<template #0>

## AST : Abstract Syntax Tree

<img src="/cve-2022-48468/ast/fixed.drawio.svg" class="h-80" />

</template>

<template #1>

## To tree-sitter query

<img src="/query_example.png" class="h-60" />

</template>

<template #2>

## To tree-sitter query

<img src="/query_example.png" class="h-60" />

<strong> > Can be used in the tree-sitter engine to make pattern-matching</strong>
</template>

<template #3>

## Tree-sitter queries

- Use tree-based approaches
- Detect type 1 and 2 code clone
- Can also detect type 3 (but with less confidences)
- Are efficient because these have been created to real time highlighting

</template>
</v-switch>