---

kanban-plugin: board

---

## TODO

- [ ] try to see if a commit of the repo dataset is marked wth a cve, and try to see if we match it
	#experience #sql
- [ ] Think about backup plan for disk
- [ ] Run the commands to make sanity check 
	- [ ] Make a verification that each queries match its sources files and doesn't match its fixed file #dev 
	#experience
- [ ] begin the exp with hyperAST #dev #experience
- [ ] use this : CyberSecEval versions 1 2 et 3 (versions successives de plus en plus grandes) 
	- [ ] run queries on it
	#dev #experience


## bug fixes



## ONGOING

- [ ] Add inside the results :
	- [x] \# files data > ## detailed language informations : add nb queries and nb files
	- [x] call the formater inside the outliers
	- [ ] \# repository dataset > all the array : call the formater
	#dev #experience
- [ ] Make a good state of the art
	- [x] prepare an obsidian template > not a good idea
	- [ ] find a way to link zotero to obsidian
	#state_of_the_art
- [ ] Inscription sur [ose](https://ose.univ-rennes1.fr/) pour donner des cours 
	- [ ] fill the form by the "employeur" 
	- [ ] Check the hour declared #administratif


## DONE

**Complete**
- [x] Fix metadata regex (exclude from the "." the ">" character to avoid to capture it if their multiple one). Also refix the field to 100 character
- [x] fix (using transaction and portal) the borowing of the client mutble multiple time #bugfix
- [x] change the "?" into "$n" #bugfix
- [x] fix NN relation (retrive the child correctly when a parent is retrieved) #bugfix
- [x] add the transaction management (postgree api) #bugfix
- [ ] check pragma on storage temp of sqlite db.
	- [ ] **PRAGMA compile_options;**
	#dev
- [x] Integrate prospector heuristic inside rust code ? > deprecated
	#dev
- [x] write script to extract and display trust level of commit (Experience 2) #dev  #experience
- [x] See mail jézéquel for given course on BMO
- [x] clean the queries witch match always or never
	#sql #experience @{2024-11-08}
- [x] develop a tool to compare size of repository (using swh ?) #dev @{2024-11-05}
- [x] develop tool to only run exp on a named repository or the cve files #dev  #experience @{2024-11-05}
- [x] restructure db code, with these different script :
	- [x] 1: script init db :
		- [x] struct + languages (inside the parent script)
		- [x] CVEs + files + CVEs_files (new crate)
		- [x] repos + commit + files(new crate)
		- [x] generate queries(new crate)
	- [ ] 2: execution of the experiment
	- [x] for file crawling :  first, inside the db_init crate, insert the repository. Then, get them and put them (as RepoEntity) to the file_crawler crate
		- [ ] Make an iterator also for the repo files ?
	#dev @{2024-11-05}
- [x] link broken in the analyze generation > not strip
	#dev #experience @{2024-11-05}
- [x] use the method get_files of the repository structure
	return the entity not the RepoFile
	#dev #experience @{2024-10-28}
- [x] Fix file (repo) crawler to exclude files with language not managed
	> #outdated put the language id to Null
	#dev #experience @{2024-10-21}
- [x] remove the results of files with extension not managed in the db (no extension, js....)
	- [ ] Put a deny list in the DB
	> #outdated just take the files of specified languages
	#dev #experience @{2024-10-21}
- [x] add tests on patch parsing #dev @{2024-10-21}
- [x] test the absence of context before if a query qtart at the beginning of the file or at the end
	- [x] add some test of multiple queries inside a single hunk
	#dev #experience @{2024-10-15}
- [x] reference the url of the CVE and attach them to their source (paper)
	- [x] Also flag the one downloaded
	- [ ] remake a full dump of the different database, and selection only the CVEs downloaded
	- [ ] put this clean (remake a clean db, with the source code clearly defined)
	- [x] understand correctly and try to remake the tool of the paper
	- [ ] consolidate CVE database
	> The clean of the db is ongoing on an other ticket
	#dev #experience @{2024-10-08}
- [x] try to have access to new dataset > password changed after the restoration of the dump #dev @{2024-10-02}
- [x] prepare presentation ~20min on the project pour swh pleniere 
	due: @{2024-10-03}
	done : @{2024-10-01} 
	#presentation
- [x] fix display bug
	- [x] fix broken link when deplacing the static html > put in the href only the file name, construct the link in the html stage
	- [x] fix date calcul : commit trust level data > time informations (make a single function to handle this)
	#dev #experience @{2024-09-27}


***

## Archive

- [x] optimize sql request to load only the sample of the language
	- [x] load in memory the sample (make a memoryplottable) #dev #experience @{2024-09-26}
- [x] Find a software/tool to make a state of the art correctly 
	#state_of_the_art @{2024-09-24}
- [x] content not found in the analyze script
	- [x] put some log (don't crash if not found)
	- [x] correct the bug
	#dev #experience  #bugfix@{2024-09-24}
- [x] make a script to run all analyze table with error message reporting #dev #experience
- [x] finish tree sitter experiment redaction 
	- [x] cve cawler
	@{2024-09-23} 
	#redaction
- [x] check in to the formations
	check [this](https://ed-matisse.doctorat-bretagne.fr/fr/formations#p-142), calculate the number of already made hours. Resume [here](obsidian://open?vault=PhD-Report&file=Formations) 
	Done [here](Formations).
	@{2024-09-23} 
	#formation #administratif
- [x] Run the commands to build the sql tables #experience @{2024-09-20} ^jkzj04
- [x] rewrite and reorganize experiences note #administratif #experience @{2024-09-20} ^ia2bu2
- [x] generalize the sql script to skip the creation of the tables if they exists and to skip already calculate data #experience #dev @{2024-09-20}

%% kanban:settings
```
{"kanban-plugin":"board","list-collapse":[false,false,false,false],"move-tags":true,"tag-colors":[{"tagKey":"#state_of_the_art","color":"rgba(137, 250, 246, 1)","backgroundColor":""},{"tagKey":"#dev","color":"rgba(250, 201, 137, 1)","backgroundColor":""},{"tagKey":"#administratif","color":"rgba(250, 137, 225, 1)","backgroundColor":""},{"tagKey":"#experience","color":"rgba(137, 250, 138, 1)","backgroundColor":""},{"tagKey":"#formation","color":"rgba(250, 137, 137, 1)","backgroundColor":""},{"tagKey":"#bugfix","color":"rgba(250, 249, 137, 1)","backgroundColor":""},{"tagKey":"#presentation","color":"rgba(255, 0, 132, 1)","backgroundColor":""},{"tagKey":"#redaction","color":"rgba(161, 220, 0, 1)","backgroundColor":""},{"tagKey":"#outdated","color":"rgba(246, 0, 0, 1)","backgroundColor":""},{"tagKey":"#sql","color":"rgba(250, 201, 137, 1)","backgroundColor":""}],"tag-sort":[{"tag":"#state_of_the_art"},{"tag":"#dev"},{"tag":"#administratif"},{"tag":"#experience"},{"tag":"#formation"},{"tag":"#bugfix"},{"tag":"#outdated"}],"move-dates":true,"new-note-template":"presentations/diverse_seminar_06_2024/node_modules/non-layered-tidy-tree-layout/screenshots/1.png","show-relative-date":true,"archive-with-date":false,"date-picker-week-start":1}
```
%%