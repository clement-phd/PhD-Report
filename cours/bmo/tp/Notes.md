[tp gdoc](https://docs.google.com/document/d/1CqhYU4_z0TmVQMnP83CE74g9KQyPFKw4/edit)

## Installation sur les linux

`dpkg-deb -x <archive.deb> <folder>`

# generation de code

- Selectionez la class
- cliquez sur le bouton en bas à droite
- Recuperer les sources dans le workspace
# deroulement
3 td (papier/crayons) + 4 tp après les vacance (sur machine)
Noté, envoyé vos projets avec des lien [gitlab de l'istic](https://gitlab2.istic.univ-rennes1.fr/) (mon at : @llahoche, mon mail : lise.lahoche@univ-rennes1.fr)
en binome
Les td serviront à realiser l'étude (q2)
Les tp à utiliser modelio et généré le code
Binome doivent etre inter groupe de tp


> presentation du projet :
> - 3 td pour faire les diagrames uml
> - 4 tp pour faire les manip modelio
> - rendu le 16 mars avec un rapport
> - Pas de modelio le premier td
> - Repartition en tp

## Trucs a dire
- PRenez des notes
- Vous serez plus noter sur les justifications que le diagramme en lui meme (meme si les deux doivent matcher et matcher au sujet)
- les associations modelio ont des sens et des fleches ! Mais surtout ne mettez pas de fleches dans votre controles ecrits pour des associations simples