# Change the java version
VScode et Java: Normalement, le jour du TP ça devrait bien se passer (ça prendra un JRE correct). Dans le cas contraire:

1. Je cite: "Lorsque tu seras en TP, il faudra que tes élèves s'assurent bien de ne pas avoir de fichier .jdk.conf dans leur home qui pointe sur une version différente, sinon il faut soit utiliser le script, soit supprimer le fichier et se deco/reco (ou peut être juste source .bashrc, je n'ai pas eu l'occasion de tester)."
2. Le script dont Cédric des admins parle est: /usr/local/etc/setjdk.sh pour choisir un jdk17 ou plus.
## clean binaire
crl+shift+p > clean java server

[correction tp gen](https://gitlab2.istic.univ-rennes1.fr/jjezeque/l2gen)
[repartition groupe](https://uniren1-my.sharepoint.com/:x:/g/personal/adrien_leroch_univ-rennes_fr/EfjJkOWN5vNIrm5dmz7lgGUBWaNC6Iy6iw1eOyKRa04O0w?rtime=oaUGFEJB3Ug)
[doc](https://docs.google.com/document/d/16OT-FMPWA87aMAzYuXwwq3SooSpdegNmNr2HtOP8Fto/edit?tab=t.0#heading=h.zba1vp10948g)
# My tp
todo : line extends A_polygone

# Groupes
Responsable communication+git (interlocuteur) : responsable de la communication dans le groupe, de la communication avec moi et manager du git 

Responsable qualité (tdd) : responsable de l'homogénéité du code et de la javadoc(vérification), qualité des tests 

Scrum master : responsable des features a ajouté et suivi de leurs implémentation, decide des features a mettre dans les presentations

>Chaque fin de séance, le resp comm m'envoie un cr des features visé/implémenté en concertation avec le scrum master 

>Pas de mail concernant le projet des autres membre du projet (absence et administration exclu)

## 1
[git](https://gitlab2.istic.univ-rennes1.fr/gmuhl/l2gen-j1)
interlocutrice : Elsa MÜHL
"Scrum Master" : Alban Peuto
## 2
[git](https://gitlab2.istic.univ-rennes1.fr/atrebel/gen-g5j2)
Interlocuteur : Agathe (Toni) TREBEL
[trello](https://trello.com/b/LNawTzr6/projet-gen)