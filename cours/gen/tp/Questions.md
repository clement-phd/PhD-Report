# TP 1

## Typo
triangle > ordre des valeurs du constructeur à inverser et description avec espace !

## contenue
arrondir les valeur (triangle et font size) ? dans les svg ? > OUI

method largeur et hauteur pour texte ?
```java
@Override
public double largeur() {
    return texte.length()*taille/1.9;
}
@Override
public double hauteur() {
    return 4*taille/3;
}
```

Bonjour,  
  
c'est complètement empirique et d'ailleurs ca ne marche pas parfaitement  
bien en svg. Si quelqu'un a une meilleure solution qui ne soit pas une  
usine à gaz...  
  
Au niveau pédagogique, c'est intéressant  pour :  
- laisser chercher les étudiant sur un problème ouvert (pour ceux qui  
comprennent qu'il y a un pb)  
- montrer aux étudiants que parfois on a des solutions approchées  
imparfaites (ou que la solution parfaite serait trop couteuse), y  
compris pour des problèmes simples, et que donc l'informatique ce n'est  
pas comme les maths (pour les victimes ayatollahs qui leur ont fait  
croire qu'on peut toujours prouver un programme).



colorier ligne ? > NON

