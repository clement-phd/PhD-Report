# CFG
Leader :
- first statement
- the target of a conditional
- a statement following a branch
For each leader, a block is the leader + all the following statement but not including the next leader

> Partial ordered instruction in the block modelized by a dependancy graph with control and data dependance

## data dependance
### Read-after_Write (RAW)
### Write after read (WAR)
### Write and write (WAW)

All instruction are dependant unless it is proven otherwise

> complete IR = CFG + DG (dependance graph, only in block)
> Doesn't capture dependance between block

# SSA (static single assignement)
each definition has a unique variable (new variable for each new assignement)
> difficult when 2 control-flow merge

## phi function
add phi_function instruction in the result blocs which as the semantic of choice of the branch
> to reconvert it, add an assignement in the preceding block using a whole new variable

this eliminate WAR and WAW
> WARN : cant have more phi parameter than predecesseor
### EXO 1

Bloc 1 :
i0 := 1
j0:= 1
k0 := 1

Bloc 2:
j4 = phi(j0, j3)
k4 = phi(k0, k3)
if k4 < 100

Bloc 3:
if j4 < 20

Bloc 4 :
return j4

bloc 5:
j1 = i0
k1 = k4 + 1

bloc 6:
j2 := k4
k2 := k4 + 2

bloc 7:
j3 := phi(j1, j2)
k3 := phi(k1, k2)

## domination tree
dominator are the node which we must go before arriving to another. A node domine itself