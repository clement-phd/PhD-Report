The queries will be generated using the modified files of each CVEs. For each CVEs, each hunk of each patch will be considered as a query (or multiple queries, see below). The queries will be generated using the tree-sitter engine, and limited to the smallest node containing the whole line range of the modification of the patch.

### When their is removed lines

A range (=context) containing the removed lines is defined. Then the smallest node containing the whole line range is taken. We define the context as 1 line before and after the removed lines, i.e. if their is only one removed line, the context will be 3 lines.

Example : 

```diff
@@ -36,7 +36,9 @@ function selectRow(checkBox)
    </script>
</head>
<body>
-<form action="<?php echo $_SERVER["PHP_SELF"];?>"  method="POST">
+<form action="<?php echo htmlentities($_SERVER['PHP_SELF']);?>"  method="POST">
<?php
```
In this example, the line range is 38-40, so the smallest node containing the whole line range is taken.

```diff
@@ -36,7 +36,9 @@ function selectRow(checkBox)
    </script>
</head>
<body>
-<form action="<?php echo $_SERVER["PHP_SELF"];?>"  method="POST">
+<form action="<?php echo htmlentities($_SERVER['PHP_SELF']);?>"  method="POST">
<?php
-<form action="<?php echo $_SERVER["PHP_SELF"];?>"  method="POST">
<?php

```

in this case, the range will be 48-52, and the smallest node containing the whole line range is taken.

### When their is no removed lines

In this case, their is no line in the original file to take, so a context must be defined to determine the line range. To be homogeneous with the previous case, the context will be 1 line before and 1 line after the line added.

Example : 

```diff
@@ -36,7 +36,7 @@ function selectRow(checkBox)
    </script>
</head>
<body>
+<form action="<?php echo htmlentities($_SERVER['PHP_SELF']);?>"  method="POST">
<?php
```

In this example, the added line is between line 38 and 39, so the line range is 38-39, and the smallest node containing the whole line range is taken.

In the case of multiple added lines, we try to agregate them, and if the range go out of the context, we take several queries by hunk.

Example : 

```diff
@@ -53,16 +53,16 @@ export function parsePatch(uniDiff, options = {}) {
   // Parses the --- and +++ headers, if none are found, no lines
   // are consumed.
   function parseFileHeader(index) {
+    const fileHeader = (/^(---|\+\+\+)\s+(.*)$/).exec(diffstr[i]);
     if (fileHeader) {
       let keyPrefix = fileHeader[1] === '---' ? 'old' : 'new';
+      const data = fileHeader[2].split('\t', 2);
+      let fileName = data[0].replace(/\\\\/g, '\\');
       if (/^".*"$/.test(fileName)) {
         fileName = fileName.substr(1, fileName.length - 2);
       }
       index[keyPrefix + 'FileName'] = fileName;
+      index[keyPrefix + 'Header'] = (data[1] || '').trim();
 
       i++;
     }
```

In this example, their is 2 queries for this hunk. The first one is from line 58 to 66, and the second one is from line 68 to 69. The 3 first added lines add a context which overlap.
### Corner case
1 : when the infected files begin by whitespace and patch begin to line 1, tree sitter will put an offset on the first node and we will not be able to extract the smallet node. This happen also at the end of the file. Semantically in our case, because we take patch line by line we must mark the query as an error, and don't try to fit the line in the tree. But this doesn't make sense as a developer point of view.
> WARN : if there is a lot, try to fix this problem
## Tree sitter usage
The method `to_sexp` of the node will be used to extract the queries from the sub tree, then a unique id is associated for this queries following the form `{cve_id}_{number}`. 

## CVE composition
Each CVEs contain then multiple queries comming from multiple files. In the following image :

![[cve_queries.drawio.svg]]

We can see the Cve1 contain 2 files : F1.1 and F1.2. And each files contains multiple queries.