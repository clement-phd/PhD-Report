# Custom "handmade" crawler

I developed a custom crawler to gather source code of CVEs. Below is the diagram of the sources crawled :
![[databases.svg]]
Then, I downloaded the ode source of the link of commit identified as "fixes" in the following forge : github.
When there are multiple link for one CVE, I tried in a random order to download the source files, and I've took the first one without error.

## Results stats
4893 CVEs in C, 3859 in c++, 1030 in Java, 3992 in Php and 251 in rust.

# Recent paper on the crawler
- ��Jafar Akhoundali et al.,  MoreFixes: A Large-Scale Dataset of CVE Fix Commits Mined through Enhanced Repository Discovery, in <�i>Proceedings of the 20th International Conference on Predictive Models and Data Analytics in Software Engineering<�/i> (PROMISE  24: 20th International Conference on Predictive Models and Data Analytics in Software Engineering, Porto de Galinhas Brazil: ACM, 2024), 42 51, <�a href="https://doi.org/10.1145/3663533.3664036">https://doi.org/10.1145/3663533.3664036<�/a>.
26,617 CVEs with full commit in a postgree datase. Use rules to ranked the urls and choose the more likely to contain the fixes. Heuristique from [here](https://ieeexplore.ieee.org/document/10381645). 
heuristique :
- ��Antonino Sabetta et al.,  Known Vulnerabilities of Open Source Projects: Where Are the Fixes?, <�i>IEEE Security &#38; Privacy<�/i> 22, no. 2 (March 2024): 49 59, <�a href="https://doi.org/10.1109/MSEC.2023.3343836">https://doi.org/10.1109/MSEC.2023.3343836<�/a>.