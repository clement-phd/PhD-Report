# Meta analyze
[[A Systematic Review on Code Clone Detection]]
## syntactic method with metric based

- Jiyong Jang, Abeer Agrawal, and David Brumley, “ReDeBug: Finding Unpatched Code Clones in Entire OS Distributions,” in 2012 IEEE Symposium on Security and Privacy (2012 IEEE Symposium on Security and Privacy (SP) Conference dates subject to change, San Francisco, CA, USA: IEEE, 2012), 48–62, https://doi.org/10.1109/SP.2012.13.
> 2012, Detect buggy code (~CVE) inside large number of line of code (700 000LoC/min), found 200 bug unpached. Work sliding a window on normalized token (remove commentary, bracket....) and calculing score on token similarity.

- Guangjie Li et al., “Test-Based Clone Detection: An Initial Try on Semantically Equivalent Methods,” IEEE Access 6 (2018): 77643–55, https://doi.org/10.1109/ACCESS.2018.2883699.> present a method to detect code clone in java, using the following method : It identifie similar method (same parameter and output) and then generate tests (with `EvoSuite`) to test the semantic of the code.

## Bug detection

### DL

- Use DL to detect C/C++ vulnerabilities > No performance consideration, but a good f1 of 0.84. Use the SATE IV Juliet Test Suite as dataset, but refine it to remove duplicate for example
Russell, Rebecca, Louis Kim, Lei Hamilton, Tomo Lazovich, Jacob Harer, Onur Ozdemir, Paul Ellingwood, and Marc McConley. “Automated Vulnerability Detection in Source Code Using Deep Representation Learning.” In 2018 17th IEEE International Conference on Machine Learning and Applications (ICMLA), 757–62. Orlando, FL: IEEE, 2018. https://doi.org/10.1109/ICMLA.2018.00120.

### linux kernel

- List of some paper on [itrans](https://coccinelle.gitlabpages.inria.fr/website/itrans.html)

- PhD report on automatic bug finding in the linux kernel (but 2017)
Iago Abal, “Effective Bug Finding.”

- [julia lawall work](https://dblp.org/pid/l/JuliaLLawall.html) on coccinelle :
	- Julia Lawall, “On the Origins of Coccinelle,” in Eelco Visser Commemorative Symposium (EVCS 2023), ed. Ralf Lämmel, Peter D. Mosses, and Friedrich Steimann, vol. 109, Open Access Series in Informatics (OASIcs) (Dagstuhl, Germany: Schloss Dagstuhl – Leibniz-Zentrum für Informatik, 2023), 18:1-18:11, https://doi.org/10.4230/OASIcs.EVCS.2023.18. > Define a tool to generalize the patchs (written in Ocaml) and made the newly created patch work on ast. We cannot genrate the patch automatically ?(if not, this isn't a good idea to use it)