# Liste des pièces

## Documents

- [ ] Carte d'identité (a imprimé)
- [X] Acte de naissance (imprimer)
- [X] Acte de mariage (imprimer)
- [x] Justificatif de domicile (imprimer)


## Témoignages

- [ ] Tushita Ramkaran, ma femme
- [ ] Lucia Vlasics, ma meilleure amie (imprimer, besoin dimprimer carte identité)
- [X] Chiara Relevat, une amie et collègue (imprimer, besoin d'imprimer carte identité)
- [ ] Dorian Lahoche, mon frère (imprimer, besoin d'imprimer carte identité)

## Preuves

- [x] Photo du prénom du bureau (à imprimer)
- [x] Photo du badge du bureau (à imprimer)
- [x] Ticket de caisse de la cafet (imprimer)
- [x] page de garde de l'article (avec le prénom souligné)
- [x] facture Hylode (à imprimer)

[](https://administrans.fr/documents/requete-changement-etat-civil-tribunal/?tribunal=Tribunal+de+Rennes&adresseTribunal=7+Rue+Pierre+Ab%C3%A9lard%2C+35000+Rennes&villeTribunal=Rennes&typeChangement=pr%C3%A9nomEtGenre&genre=f%C3%A9minin&nouveauxPr%C3%A9noms=Lise+Jenna+Cl%C3%A9mence+C%C3%A9leste&pr%C3%A9nomsActuels=Cl%C3%A9ment+Claude+Martial&nom=Lahoche&dateNaissance=2001-01-06&lieuNaissance=Rennes+%2835%29&nationalit%C3%A9=fran%C3%A7aise&adresse=16+rue+Aim%C3%A9e+Antignac%2C+Appartement+107%2C+Rennes+35000&email=lise%40lahoche.com&t%C3%A9l%C3%A9phone=0652403235&situationProfessionnelle=Doctorante%2FEtudiante&situationFamiliale=Mari%C3%A9e&contexte=Je+suis+une+personne+transgenre%2C+c%27est-%C3%A0-dire+que+mon+genre+de+naissance+n%27est+pas+en+ad%C3%A9quation+avec+mon+genre+r%C3%A9el.%0A%0ADepuis+de+nombreuses+ann%C3%A9es%2C+je+me+pr%C3%A9sente+et+vis+quotidiennement+en+tant+que+femme%2C+tant+dans+mes+relations+familiales+qu%27amicales+ou+professionnelles.%0ALes+personnes+qui+me+cotoient+au+quotidien+utilisent+cette+identit%C3%A9+de+genre+et+l%27acceptent+parfaitement.%0ADe+plus%2C+l%27utilisation+de+mon+pr%C3%A9nom+d%27usage+n%27%C3%A9tant+pas+reconnu%2C+il+m%27est+difficile+de+totalement+m%27%C3%A9panouir+en+tant+que+femme.%0A%0ACette+transition+sociale+s%27accompagne+%C3%A9galement+d%27autres+changements+li%C3%A9s+%C3%A0+mon+apparence%2C+notamment+vestimentaires+pour+me+rapprocher+de+mon+genre+r%C3%A9el.%0A%0AJe+suis+satisfaite+de+ma+transition+et+je+me+sens+plus+%C3%A9quilibr%C3%A9e+et+en+accord+avec+moi-m%C3%AAme+depuis+que+j%27ai+entrepris+ces+changements.%0A%0ACependant%2C+je+suis+souvent+contrainte+de+r%C3%A9v%C3%A9ler+ma+transidentit%C3%A9+%C3%A0+des%0Atiers+%28administrations%2C+banques%2C+milieu+professionnel%2C+agents+de%0As%C3%A9curit%C3%A9%2C+contr%C3%B4leurs+des+transports%2C+a%C3%A9roports%2C+op%C3%A9rateurs%0At%C3%A9l%C3%A9phoniques%2C+passe+sanitaire+Covid-19%2C+...%29+qui+me+demandent+des%0Ajustifications+allant+parfois+jusqu%27%C3%A0+exiger+mon+acte+de+naissance%0Aet%2Fou+des+lettres+m%C3%A9dicales+pathologisantes%2C+ce+qui+constitue+une%0Agrave+atteinte+%C3%A0+mon+intimit%C3%A9+et+%C3%A0+ma+vie+priv%C3%A9e.+De+plus%2C+ma%0Asituation+civile+actuelle+m%27a+d%C3%A9j%C3%A0+entrav%C3%A9+l%E2%80%99acc%C3%A8s+%C3%A0+certains+services%0Apublics+%3A+refus+d%E2%80%99acc%C3%A8s+aux+soins+%28refus+de+d%C3%A9livrance+de+m%C3%A9dicament%0Adans+des+pharmacies%29%2C+refus+de+d%C3%A9livrance+de+colis+%C3%A0+La+Poste%2C%0Aimpasses+informatiques...+Plusieurs+emplois+m%27ont+%C3%A9galement+%C3%A9t%C3%A9%0Arefus%C3%A9s+pour+les+m%C3%AAmes+raisons.%0A%0APour+toutes+ces+raisons%2C+j%27aimerais+que+mon+%C3%A9tat+civile+refl%C3%A8te+qui+je+suis.&contexteExemple=&actes%C3%80Mettre%C3%80Jour=l%27acte+de+mariage%0A++++++&justificatifsRequ%C3%AAte=Copie+int%C3%A9grale+de+mon+acte+de+naissance%0APhotocopie+de+ma+carte+nationale+d%27identit%C3%A9%0AJustificatif+de+domicile%0AT%C3%A9moignage+et+carte+d%27identit%C3%A9+recto+verso+de+Lucia+Vlasics%2C+meilleure+amie%0AT%C3%A9moignage+et+carte+d%27identit%C3%A9+recto+verso+de+Dorian+Lahoche%2C+fr%C3%A8re%0AT%C3%A9moignage+et+carte+d%27identit%C3%A9+recto+verso+de+Chiara+Relevat%2C+amie+et+coll%C3%A8gue%0APhoto+du+pr%C3%A9nom+utilis%C3%A9+pour+indiquer+mon+bureau%0APhoto+du+badge+d%27entr%C3%A9e+de+mon+bureau%0ATicket+de+caisse+de+la+caf%C3%A9tariat%0APage+de+garde+de+mon+article+%22Large+Scale+Heap+Dump+Embedding+for+Machine+Learning%3A+Predicting+OpenSSH+Key+Locations%22%0AFacture+du+centre+esth%C3%A9tique+Hylode&villeDocument=Rennes&dateDocument=2025-01-13)