# CVE catalogue and repo
![workflow](list_cve.drawio.svg)

## Meta analyze of code clone detection methods in 2019

Not focus on CVE so the dataset doesn't contain it.

## hand made

> generation from mitre (crawling), use ml to detect them, syntaxe of the vulnerability not standard (no AST....)
> The list of the CVE can be found [here](https://www.cve.org/Downloads), it use [CVRF](https://cve.mitre.org/cve/cvrf.html) syntaxe (XML derived) or JSON.
> The patch aren't clear for all the CVE

## Dataset in paper
 
### Detecting ‘0-Day’ Vulnerability: An Empirical Study of Secret Security Patch in OSS (2019)

Etude vraiment tres interessante (ml sur un dataset de CVE crawler) : Xinda Wang et al., “Detecting ‘0-Day’ Vulnerability: An Empirical Study of Secret Security Patch in OSS,” in 2019 49th Annual IEEE/IFIP International Conference on Dependable Systems and Networks (DSN) (2019 49th Annual IEEE/IFIP International Conference on Dependable Systems and Networks (DSN), Portland, OR, USA: IEEE, 2019), 485–92, https://doi.org/10.1109/DSN.2019.00056.

> Crawl cve database, extract the github patch, and try to load other and identify patch with character like "@@@" ou "+++". Only 600 cves found with the "try to identify patch" method.
Dataset : https://github.com/SecretPatch/Dataset

### A Ground-Truth Dataset of Real Security Patches (2021)

Sofia Reis and Rui Abreu, “A Ground-Truth Dataset of Real Security Patches” (arXiv, October 18, 2021), http://arxiv.org/abs/2110.09635. > 8057 security-relevant commits from cve [site](https://www.cvedetails.com/) which seem updated. The craxler is available [here](https://github.com/TQRG/security-patches-dataset), identify commit hash, changed files, and the cve id. The dataset is [here](https://github.com/TQRG/security-patches-dataset/tree/main/sources), 2022.

NVD (☠️ 7316 CVEs) - CVEs data provided by the National Vulnerability Database from 2002 to 2022.
OSV (☠️ 4125 CVEs) - Project maintained by Google.

### CVE-Assisted Large-Scale Security Bug Report Dataset Construction Method (2020)

Xiaoxue Wu et al., “CVE-Assisted Large-Scale Security Bug Report Dataset Construction Method,” Journal of Systems and Software 160 (February 2020): 110456, https://doi.org/10.1016/j.jss.2019.110456.

try to use ML to extract bug report (= description) from cve database. > Not very usefull in our case : The final goal of the paper is to separate bug report from security bug report (SBR).

### CVEfixes: Automated Collection of Vulnerabilities and Their Fixes from Open-Source Software (2021)

Guru Bhandari, Amara Naseer, and Leon Moonen, “CVEfixes: Automated Collection of Vulnerabilities and Their Fixes from Open-Source Software,” in Proceedings of the 17th International Conference on Predictive Models and Data Analytics in Software Engineering (PROMISE ’21: 17th International Conference on Predictive Models and Data Analytics in Software Engineering, Athens Greece: ACM, 2021), 30–39, https://doi.org/10.1145/3475960.3475985.

> 5365 cve from 2002 to 2020, with the patch in a structured dataset (sqlite) [here](https://github.com/secureIT-project/CVEfixes)

## vulnerability prediction dataset meta analyse (2024)

Matteo Esposito and Davide Falessi, “VALIDATE: A Deep Dive into Vulnerability Prediction Datasets,” Information and Software Technology 170 (June 2024): 107448, https://doi.org/10.1016/j.infsof.2024.107448.

> Meta analyse of the dataset used in vulnerability prediction, provide dataset informations [here](https://validatetool.org/) TODO : explore them

### DiverseVul: A New Vulnerable Source Code Dataset for Deep Learning Based Vulnerability Detection (2023)

Yizheng Chen et al., “DiverseVul: A New Vulnerable Source Code Dataset for Deep Learning Based Vulnerability Detection,” in Proceedings of the 26th International Symposium on Research in Attacks, Intrusions and Defenses (RAID 2023: The 26th International Symposium on Research in Attacks, Intrusions and Defenses, Hong Kong China: ACM, 2023), 654–68, https://doi.org/10.1145/3607199.3607242. 

> c++/cdataset from 7,514 commits from [snyk](https://snyk.io/) and [bugzilla.redhat.com](https://bugzilla.redhat.com/), and apply an heuristique to identify security related patch. dataset [here](https://drive.google.com/file/d/12IWKhmLhq7qn5B_iXgn5YerOQtkH-6RG/view?pli=1)

> Cannot be used, only vulnerable function are in the dataset

### [OSV](https://github.com/google/osv.dev)

This a project from google to track the CVEs in the open source world. It's a database structured.


## crawling
Their is some way to crawl the CVEs, because the vast majority of them aren't formated in a stardard way.

- [openCVE](https://www.opencve.io/welcome)
- [osv](https://osv.dev/)
- [big vul](https://github.com/ZeoVan/MSR_20_Code_vulnerability_CSV_Dataset), Fan et al., “A C/C++ Code Vulnerability Dataset with Code Changes and CVE Summaries.” > crawler in python with 4400 cves in c/c++ > use [this site](https://www.cvedetails.com/browse-by-date.php) but doesnt seem updated