##   Detections of code clones


## Meta analyze of code clone detection methods in 2019
- Qurat Ul Ain et al., “A Systematic Review on Code Clone Detection,” IEEE Access 7 (2019): 86121–44, https://doi.org/10.1109/ACCESS.2019.2918202.
> Compare 54 study between 2013 and 2018 on code clone detection (CCD) and separe it in 6 category : 
![method summary from 8](legacy/state_of_art/code_clones_detection/img/meta_analyze/ccd_def/ccd_detection_methods_approaches.png)
    - TEXTUAL APPROACHES (12) : It detect type1 clones more effectively [64]. However, these approaches can also identify type2 and type3 clones [8].
    - LEXICAL APPROACHES (8) : Lexical approaches are also known as token-based approaches and able to identify type 2 clones efficiently [64]. However, they can also uncover type1 and type3 clones [8].
    - TREE-BASED APPROACHES (3) : They are most effective for the detection of type3 clones [64]. However, they have the ability to detect type1, type2, and type4 clones [8].
    - METRIC BASED APPROACHES (7) : They can detect type3 clones effectively [64]. However, they can also uncover type1, type2 and type4 clones.
    - SEMANTIC APPROACHES (7) : Use execution or PDG (Program Dependence Graph, the vertices are assignment and control flow predicate) It have the ability to detect type1, type2, type3 and type4 clones. They are mainly used to uncover semantic or type 4 clones [64].
    - HYBRID APPROACHES (17) : It is a combination of two or more aforementioned techniques e.g. (Textual, Lexical, Syntactic or Semantic)
![date_repartition_litterature_review.png](legacy/state_of_art/code_clones_detection/img/meta_analyze/date_repartition_litterature_review.png)

Then, use 4 different dataset to tests them on 4 type of bug :
    - Exact clones (Type 1): Identical code segments except for changes in comments, layouts and whitespaces.
    - Renamed clones (Type 2): Code segments which are syntactically or structurally similar other than changes in comments, identifiers, types, literals, and layouts. These clones are also called parameterized clones.
    - Near Miss clones (Type 3): Copied pieces with further modification such as addition or removal of statements and changes in whitespaces, identifiers, layouts, comments, and types but outcomes are similar. These clones are also known as gapped clones.
    - Semantic clones (Type 4): More than one code segments that are functionally similar but implemented by different syntactic variants

![method_summary.png](legacy/state_of_art/code_clones_detection/img/meta_analyze/method_summary.png)
![correctness_results.png](legacy/state_of_art/code_clones_detection/img/meta_analyze/correctness_results.png)
![time_results.png](legacy/state_of_art/code_clones_detection/img/meta_analyze/time_results.png)

### Tree based approach :

(
- [6] T. Smith, “Waterman ms: Identification of common molecular subsequences,” Journal of Molecular Biology, vol. 147, no. 1, p. 195197, 1980. > Define a metric to get the length between two sequence of character (Smith Waterman algorithm)
)

[44] Y. Yang, Z. Ren, X. Chen, and H. Jiang, ‘‘Structural function based code clone detection using a new hybrid technique,’’ in Proc. IEEE 42nd Annu. Comput. Softw. Appl. Conf. (COMPSAC), Jul. 2018, pp. 286–291. > We propose a novel function based code clone detection method which combines a variant AST and Smith Waterman [6]. More specifically, each function is first represented as a variant of AST in which each node actually corresponds to one defined node type (e.g., int, double, String, or constant) rather than a specific identifier, literal, or operator (e.g., =, +, -, or *). Then, we traverse the entire tree in a depth-first order and consequently generate an overall sequence for each specific function. After that, we leverage the Smith Waterman algorithm to construct a scoring matrix to gain the similarity score between two candidate sequences. Finally, we calculate the similarity between the two sequences by using the similarity score. In such a way, we can eventually determine whether they are code clones. > Only function in java are detected

[45] J. Pati, B. Kumar, D. Manjhi, and K. K. Shukla, ‘‘A Comparison Among ARIMA, BP-NN, and MOGA-NN for Software Clone Evolution Predic- tion,’’ IEEE Access, vol. 5, pp. 11841–11851, 2017. > Use AST (tool cloneDr) and neural network to detect code clone

[46] !!!! S. Chodarev, E. Pietriková, and J. Kollár, ‘‘Haskell clone detection using pattern comparing algorithm,’’ in Proc. 13th Int. Conf. Eng. Mod. Electr. Syst. (EMES), Jun. 2015, pp. 1–4 > use pattern recognition algorith grouping similar code into groups and making some assumption to optimize it

### Hybrid approach :

![hybrid method summary](legacy/state_of_art/code_clones_detection/img/meta_analyze/hybrid_method_summary.png)

Method with AST/PDG : 61, 62, 63, 65, 67, 68, 70, 72, 75, 76
Tool : Deckard, CloneDr, Duplix


[61] Roopam and G. Singh, ‘‘To enhance the code clone detection algorithm by using hybrid approach for detection of code clones,’’ in Proc. Int. Conf. Intell. Comput. Control Syst. (ICICCS), Jun. 2017, pp. 192–198. [Online]. Available: https://ieeexplore.ieee.org/abstract/document/8250708 > Use PSG with a metric based filter (MTB, dont know what it is) at the start to detetct clone

[62] M. R. H. Misu and K. Sakib, ‘‘Interface driven code clone detection,’’ in Proc. 24th Asia–Pacific Softw. Eng. Conf. (APSEC), Dec. 2017, pp. 747–748. > use a combination of method signature with jaccard metrics to calculate the similarities

[63] A. Sheneamer and J. Kalita, ‘‘Semantic clone detection using machine learning,’’ in Proc. 15th IEEE Int. Conf. Mach. Learn. Appl. (ICMLA), Dec. 2016, pp. 1024–1028. > Compare function using feature extracted in the combination of the textual, AST and PDG representation of the code, then apply classifiers.

[65] M. R. H. Misu, A. Satter, and K. Sakib, ‘‘An exploratory study on interface similarities in code clones,’’ in Proc. 24th Asia–Pacific Softw. Eng. Conf. Workshops (APSECW), Dec. 2017, pp. 126–133. > duplicate of 62

[67] !!!! A. Sheneamer, S. Roy, and J. Kalita, ‘‘A detection framework for semantic code clones and obfuscated code,’’ Expert Syst. Appl., vol. 97, pp. 405–420, May 2018 > On Java only, use AST, PDG and bytecode compiled from code to etact feature and apply machin learning

[68] T. Matsushita and I. Sasano, ‘‘Detecting code clones with gaps by function applications,’’ in Proc. ACM SIGPLAN Workshop Partial Eval. Program Manipulation, 2017, pp. 12–22. > focus on function application in functionnal languages (Haskell, erlang). Propose an algorithm to detect it on the AST and a token list (constructed with the source code)

[69] !!!! E. Kodhai and S. Kanmani, ‘‘Method-level code clone detection through LWH (Light Weight Hybrid) approach,’’ J. Softw. Eng. Res. Develop., vol. 2, no. 1, p. 12, 2014 > Mix standardisation of the ast with PDG to determine metrics like No. of Return Statements... to detect clone

[70] R. Tekchandani, R. Bhatia, and M. Singh, ‘‘Semantic code clone detection for Internet of things applications using reaching definition and liveness analysis,’’ J. Supercomput., vol. 74, no. 9, pp. 4199–4226, 2018 > NEED ACCESS

[72] H. Nasirloo and F. Azimzadeh, ‘‘Semantic code clone detection using abstract memory states and program dependency graphs,’’ in Proc. 4th Int. Conf. Web Res. (ICWR), Apr. 2018, pp. 19–27 > Use normalization, hash comparaison and pdg comparaison to build code similarities and compare it

[75] !!!! M. White, M. Tufano, C. Vendome, and D. Poshyvanyk, ‘‘Deep learning code fragments for code clone detection,’’ in Proc. 31st IEEE/ACM Int. Conf. Automated Softw. Eng., 2016, pp. 87–98. > Use 2 neural network (one to make an embedding of the code to test, and one to detect clone with the embedding and a combination of a syntactic and ast based method) to detect clone (use autoencoder on AST)

[76] C. Ragkhitwetsagul J. Krinke, and D. Clark, ‘‘A comparison of code similarity analysers,’’ Empirical Softw. Eng., vol. 23, no. 4, pp. 2464–2519, 2018. > Other meta analyze of CCD > No ast method

#### Tools

- [deckard](https://github.com/skyhover/Deckard) : Lingxiao Jiang et al., “DECKARD: Scalable and Accurate Tree-Based Detection of Code Clones,” in 29th International Conference on Software Engineering (ICSE’07) (29th International Conference on Software Engineering, Minneapolis, MN, USA: IEEE, 2007), 96–105, https://doi.org/10.1109/ICSE.2007.30. > Calculate vector of caracteristic of the ast of the code and then compare them with euclidian distance.

- CloneDr (not free) : Ira D Baxter and Dale Churchett, “Using Clone Detection to Manage a Product Line,” n.d.> Use a combination of abstraction (rename variable to be the same) and AST matching to detect clone

- [Duplex](https://github.com/zirkonit/duplex) : AST based code detector for elixir 

### Biblio Meta analyze

[3] !!!! : J. Krinke, ‘‘Identifying similar code with program dependence graphs,’’ in Proc. 8th Work. Conf. Reverse Eng., Oct. 2001, pp. 301–309. > Define PDG (Program Dependence Graph, the vertices are assignment and control flow predicate) to detect code clone. To do so, construct the maximal subgraph in common.

[7] C. K. Roy, R. J. Cordy, and R. Koschke, ‘‘Comparison and evaluation of code clone detection techniques and tools: A qualitative approach,’’ Sci. Comput. Program., vol. 74, no. 7, pp. 470–495, 2009. > Old meta-analyze on code clone detection

[8] N. Saini and S. Singh, ‘‘Code clones: Detection and management,’’ Procedia Comput. Sci., vol. 132, pp. 718–727, Jun. 2018. > A review of code clone detection and management techniques > lexical approach, tokenization, then standardisation, then matching, and finally output line number

[17] N. Davey, P. Barson, S. Field, R. Frank, and D. Tansley, ‘‘The development of a software clone detector,’’ in International Journal of Applied Software Technology. London, U.K.: University of Hertfordshire Research Archive, 1995. > use neural network to detect code clone (transforming the CC into fixed size vector)

[64] T. Vislavski, G. Rakić, N. Cardozo, and Z. Budimac, ‘‘LICCA: A tool for cross-language clone detection,’’ in Proc. IEEE 25th Int. Conf. Softw. Anal., Evol. Reeng. (SANER), Mar. 2018, pp. 512–516. > LICCA is a tool for cross-language clone detection, using a cross-language AST (Set of Software Quality Static Analysers (SSQSA) platform)

### Conclusion of the meta analyze

- good ways to define technique of code clone detection
- good way to define code clone type

- a lot of AST and PDG (tree-based) method have been developed, but lake of time performance concern. 
- many of the method are language specific, and can't be used on other language
- The hybrid method are the most effective, but even more time consuming, often rely on ml and dl techniques
- lack of standardisation of the dataset used (no dataset using CVE)

- best analyzed paper (related to my work) are : [64], [67], [69], [75], [3], [46]

## syntactic method with metric based

- Jiyong Jang, Abeer Agrawal, and David Brumley, “ReDeBug: Finding Unpatched Code Clones in Entire OS Distributions,” in 2012 IEEE Symposium on Security and Privacy (2012 IEEE Symposium on Security and Privacy (SP) Conference dates subject to change, San Francisco, CA, USA: IEEE, 2012), 48–62, https://doi.org/10.1109/SP.2012.13.
> 2012, Detect buggy code (~CVE) inside large number of line of code (700 000LoC/min), found 200 bug unpached. Work sliding a window on normalized token (remove commentary, bracket....) and calculing score on token similarity.

- Guangjie Li et al., “Test-Based Clone Detection: An Initial Try on Semantically Equivalent Methods,” IEEE Access 6 (2018): 77643–55, https://doi.org/10.1109/ACCESS.2018.2883699.> present a method to detect code clone in java, using the following method : It identifie similar method (same parameter and output) and then generate tests (with `EvoSuite`) to test the semantic of the code.

## Bug detection

### DL

- Use DL to detect C/C++ vulnerabilities > No performance consideration, but a good f1 of 0.84. Use the SATE IV Juliet Test Suite as dataset, but refine it to remove duplicate for example
Russell, Rebecca, Louis Kim, Lei Hamilton, Tomo Lazovich, Jacob Harer, Onur Ozdemir, Paul Ellingwood, and Marc McConley. “Automated Vulnerability Detection in Source Code Using Deep Representation Learning.” In 2018 17th IEEE International Conference on Machine Learning and Applications (ICMLA), 757–62. Orlando, FL: IEEE, 2018. https://doi.org/10.1109/ICMLA.2018.00120.

### linux kernel

- List of some paper on [itrans](https://coccinelle.gitlabpages.inria.fr/website/itrans.html)

- PhD report on automatic bug finding in the linux kernel (but 2017)
Iago Abal, “Effective Bug Finding.”

- [julia lawall work](https://dblp.org/pid/l/JuliaLLawall.html) on coccinelle :
	- Julia Lawall, “On the Origins of Coccinelle,” in Eelco Visser Commemorative Symposium (EVCS 2023), ed. Ralf Lämmel, Peter D. Mosses, and Friedrich Steimann, vol. 109, Open Access Series in Informatics (OASIcs) (Dagstuhl, Germany: Schloss Dagstuhl – Leibniz-Zentrum für Informatik, 2023), 18:1-18:11, https://doi.org/10.4230/OASIcs.EVCS.2023.18. > Define a tool to generalize the patchs (written in Ocaml) and made the newly created patch work on ast. We cannot genrate the patch automatically ?(if not, this isn't a good idea to use it)