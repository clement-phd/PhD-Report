# Modelisation of codes

etude sur la modelisation de faille de securité : https://ieeexplore.ieee.org/stamp/stamp.jsp?tp=&arnumber=4159681

> very simple models state/transition and automatic testing. This is a bit abstract (no real application)

Zhao, Jingling, Kunfeng Xia, Yilun Fu, and Baojiang Cui. “An AST-Based Code Plagiarism Detection Algorithm.” In 2015 10th International Conference on Broadband and Wireless Computing, Communication and Applications (BWCCA), 178–82. Krakow, Poland: IEEE, 2015. https://doi.org/10.1109/BWCCA.2015.52.


Yang et al., “Asteria.”

> Use Ast and dl (tree-LSTM) to detect security vulnerability, [code](https://github.com/Asteria-BCSD/Asteria)


## AST + PDG + CFG
 > Fabian Yamaguchi et al., “Modeling and Discovering Vulnerabilities with Code Property Graphs,” in 2014 IEEE Symposium on Security and Privacy (2014 IEEE Symposium on Security and Privacy (SP), San Jose, CA: IEEE, 2014), 590–604, https://doi.org/10.1109/SP.2014.44.

 This study present a graph combining AST, PDG and CFG to detect vulnerability. Very cool, but the conversion of the vulnerabilities into a traversal of the graph is not trivial, and cannot be done automatically easily. 