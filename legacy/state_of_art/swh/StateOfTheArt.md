# Software heritage

the [API](https://archive.softwareheritage.org/api/), a local [instance](https://docs.softwareheritage.org/devel/getting-started/index.html), [python tool lister](https://forge.softwareheritage.org/source/swh-lister/)

Pietri, Spinellis, and Zacchiroli, “The Software Heritage Graph Dataset.”

> Make a DAG of Software heritage (on the metadata and the files), make query like "what is the most used name"

Romain Lefeuvre et al., “Fingerprinting and Building Large Reproducible Datasets,” in Proceedings of the 2023 ACM Conference on Reproducibility and Replicability (ACM REP ’23: 2023 ACM Conference on Reproducibility and Replicability, Santa Cruz CA USA: ACM, 2023), 27–36, https://doi.org/10.1145/3589806.3600043.

> Make a tool to request more easily the graph of swh, and create dataset

## Romain project
Romain labelize the graph of swh with the data of the [cve](https://osv.dev/) to be able to list all the repository affected by the cve (which use the interval infected).
