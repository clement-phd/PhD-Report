ŧ# 1st Experiment

![graphique of the experiment](first_experiment.drawio.svg)

Objectif : First naive approach to detect CVE inside code source

# 1st test
The goal is to detect a list of CVEs within files. To achieve this, we require a CVE database, which we construct using openCVE, a tool that aggregates all known CVEs from three databases into a semi-structured PostgreSQL database, defined as such due to a JSON field that isn't converted. The initial approach to detecting CVEs within files is to employ AST (Abstract Syntax Tree) matching. A well-known tool for this purpose is Tree-sitter, which can construct ASTs from various languages and simplifies tree matching through a feature known as "query". The experiment is divided into three parts:

- Isolation of the code involved with the CVE.
- Generation of the query.
- Application of the query to a test file.

To isolate the code from the CVE, we need the source code and associated files for the CVE. These are converted from the earlier collected CVE database. This conversion involves extracting all CVEs with a pull request from GitHub to obtain the files and information from the pull request. For each CVE, we acquire some files before and after the fix. The next step is to isolate the code that has changed in the files. We utilize the patch from the pull request to identify a window of code that has changed and isolate these lines from the original (before the fix) files. There can be several patches for each file, leading to multiple segments for each file.

Then, to generate the queries, we transform the isolated code into a Tree-sitter AST and use a built-in tool to extract the query.

Finally, we synthesize all the queries from all the CVEs into a single query file and employ it in the highlight feature of Tree-sitter to detect the CVEs.

## Database creation and files extraction
The construction of the database yielded 200,000 CVEs, which were reduced to 2,544 CVEs after filtering out those without a GitHub pull request patch URL. Subsequently, we created a script to download the 1.2 GB of files from these pull requests. While this isn't a substantial amount, one approach to increase this number is to extract from the [osv.dev](https://osv.dev/) database and retrieve the versions of the code that are fixed, along with the patches to retrieve the unfixed code.


## Isolation of the CVE code
This step is not straightforward. The goal of this part is to identify the code responsible for the CVE. 

### Syntaxic approach

The first approach we chose was to get the lines between the first and last line removed from each part of the patches. This isolates the removed or changed code to patch the CVE and is a good heuristic to determine a context for this CVE. This led to several problems and adaptations. First, there will be multiple CVE codes for one CVE. Then, because we use syntactic patches, we break some of the ASTs and can't convert them to tree-sitter AST (this can be solved using tree difference, see the detailed part). Finally, we cannot define a clear context when the patches only add lines (we can't define a good context in the original files). We leverage these problems removing the brocken AST and wildcard the nodes.

### AST approach

The second approach we choose was to construct the ast of the infected file, then get the minimal parent node which contain the patch. This fix the problem of the broken AST, but doesn't fix the context problem. This also led to a new problem which are that some query will be all the file, if for example a patch contain the las `}` of a root function, which doesn't make sense. 


## Query creation
We can then use tree sitter query to detect these CVE inside files. We convert each CVE code found in the last step into a query tree-sitter (using tree sitter ast and `tosexp` tool) and write them into one file per queries, and one file which contain all the queries call multi line query. Then we give them to the tree-sitter motor and get the matches.

NOTE : Tree-sitter doesn't allow the parsing of too long query, we choose to cut multiline queries into several part to leverage this problem, and execute them one before another.

## Tests





# Notes

Tree-sitter querries are limited in size.

# TODO
- [ ] analyse repo stats
- [ ] benchmark : zip en remove the old file after each instance
- [ ] analyze : read zip files

- [ ] verify that there is no double in th patch url
- [ ] add to the protocole stats calculations idea, and queries eliminations and proportions to detect cve
- [ ] rewrite cleanly the first experiment results
- [ ] Save result on the nas (tatooine)
- [ ] add filter on query based on the result of the query (with repartitions filter, if the query math all, or if the query match too rarely, elimine it)
- [ ] add similiraty calcul (portion of querry matched)


# Find the CVE

Use the following [cite](https://www.opencve.io/cve?vendor=rust-lang) (Scrap 3 site : NVD, Mitre and CVE.ORG)

> [Here](https://github.com/opencve/opencve/blob/master/opencve/commands/imports/cve.py) is the CVE scapping from NVD, [here](https://github.com/opencve/opencve/blob/master/opencve/commands/imports/cwe.py) the scrapping from mitre

Candidate :

- [CVE-2023-22895](https://www.opencve.io/cve/CVE-2023-22895) -> this one is multiline, not over complicated, consist to add call to `.min()` function, in rust, with a [pull request](https://github.com/alexcrichton/bzip2-rs/pull/86)

- [CVE-2019-10269](https://www.opencve.io/cve/CVE-2019-10269) -> Multi-line too, fix consist on adding line, not modifying it, in c, with a [pull request](https://github.com/lh3/bwa/pull/232)
- [CVE-2021-37519](https://www.opencve.io/cve/CVE-2021-37519) -> Multi-line too, 10 lines, consist to add branch to if, and some condtions, in c, with a [pull request](https://github.com/memcached/memcached/pull/806)

