# TODO

- [ ] clean the queries (remove the pathologique one like only comments). 
    - [X] remove easily identifiable patterns (like only comments)
    - [ ] Make hand made source file with pathologique pattern and eliminate the ones xhoch match

- [X] Make a pipeline to download files from repository and add it to the dataset, and fix in time the experiment
    - [X] isolate one time the files downloaded
    - [X] integrate it with the full dataset
    - [ ] fix bug


- [X] Update the analyze tool to handle this much of data
    - [X] handle compression
    - [X] load as stream, not in memory

- [X] Fix benchmark reprise

- [ ] Add in the preparation of dataset, cve_code, link to file.
    - [ ] put in the queries agregation the right boolean flag