# Queries selection protocol

## Goal

The goal of this experiment are :
- select the queries (not too much false positives, and eliminated if it doesn't match its own infected file)
- then select the cve containing enough good queries to be detected
- evaluate the speed of the queries if we use the tree-sitter engine


## Dataset

The dataset will be the one crawled from the cve database repository.

## Queries Generation

The queries will be generated using the modified files of each CVEs. For each CVEs, each hunk of each patch will be considered as a query (or multiple queries, see below). The queries will be generated using the tree-sitter engine, and limited to the smallest node containing the whole line range of the modification of the patch.

### When their is removed lines

A range (=context) containing the removed lines is defined. Then the smallest node containing the whole line range is taken. We define the context as 1 line before and after the removed lines, i.e. if their is only one removed line, the context will be 3 lines.

Example : 

```diff
@@ -36,7 +36,9 @@ function selectRow(checkBox)
    </script>
</head>
<body>
-<form action="<?php echo $_SERVER["PHP_SELF"];?>"  method="POST">
+<form action="<?php echo htmlentities($_SERVER['PHP_SELF']);?>"  method="POST">
<?php
```
In this example, the line range is 38-40, so the smallest node containing the whole line range is taken.

```diff
@@ -36,7 +36,9 @@ function selectRow(checkBox)
    </script>
</head>
<body>
-<form action="<?php echo $_SERVER["PHP_SELF"];?>"  method="POST">
+<form action="<?php echo htmlentities($_SERVER['PHP_SELF']);?>"  method="POST">
<?php
-<form action="<?php echo $_SERVER["PHP_SELF"];?>"  method="POST">
<?php

```

in this case, the range will be 48-52, and the smallest node containing the whole line range is taken.

### When their is no removed lines

In this case, their is no line in the original file to take, so a context must be defined to determine the line range. To be homogeneous with the previous case, the context will be 1 line before and 1 line after the line added.

Example : 

```diff
@@ -36,7 +36,7 @@ function selectRow(checkBox)
    </script>
</head>
<body>
+<form action="<?php echo htmlentities($_SERVER['PHP_SELF']);?>"  method="POST">
<?php
```

In this example, the added line is between line 38 and 39, so the line range is 38-39, and the smallest node containing the whole line range is taken.

In the case of multiple added lines, we try to agregate them, and if the range go out of the context, we take several queries by hunk.

Example : 

```diff
@@ -53,16 +53,16 @@ export function parsePatch(uniDiff, options = {}) {
   // Parses the --- and +++ headers, if none are found, no lines
   // are consumed.
   function parseFileHeader(index) {
+    const fileHeader = (/^(---|\+\+\+)\s+(.*)$/).exec(diffstr[i]);
     if (fileHeader) {
       let keyPrefix = fileHeader[1] === '---' ? 'old' : 'new';
+      const data = fileHeader[2].split('\t', 2);
+      let fileName = data[0].replace(/\\\\/g, '\\');
       if (/^".*"$/.test(fileName)) {
         fileName = fileName.substr(1, fileName.length - 2);
       }
       index[keyPrefix + 'FileName'] = fileName;
+      index[keyPrefix + 'Header'] = (data[1] || '').trim();
 
       i++;
     }
```

In this example, their is 2 queries for this hunk. The first one is from line 58 to 66, and the second one is from line 68 to 69. The 3 first added lines add a context which overlap.

## Queries testing

### Dataset

The dataset will be all the infected files and fixed files of the CVEs. We can also use files from git repository choosen.

### Method

Each queries will be tested against each of the files of the dataset.

### Queries selection

x_n the queries, y_n the files from the cves' files, z_n the files from the git repository. We will not eliminate any query because we think that there will be not a lot of abnormal queries, and the abnormale one will be not taken in account when there is multiple queries in one CVE (see how the cve are detected, CVE selections). If there are too much we can use some countermesure like the following :

#### hand selection

First, we can see which queries are abnormale by hand. We can see if the queries are too general or too specific. We can also see if the queries are too long or too short.

#### normal distribution

Secondly, we can make statistic. For each query x_i, we will calculate the number of matches for each files y_1...y_n...z_1...z_n . We will then calculate the mean and the standard deviation of the number of matches. We will then eliminate the queries that have a number of matches greater than the mean + 3 * standard deviation. We see the queries matches like a normal distribution. We can plot it with histograme (discrete => continue) number of x_i which have a p_i between X and X+1 for example.

> WARN : in this method, we assume that the queries will follow a normal law, which can be wrong.

### CVEs selection

Each query will match minimum one time (the file it come from). But we cannot assume the number of matches of a query in the other files. We must determine a protocol to eliminate outliers. We can say if 90% of the queries are positif match the CVE will be validate (this number can evolve with the experiments).

## Graphs an data

### dataset

- number of queries per language
- number of queries per CVE
- number of cve per language
- time to load the queries distribution
- ponderate time to load the queries by language

### execution of the queries

- execution time of the queries by number of lines of the file
- execution time of the queries by number of characters of the file
- execution time by number of matches

### correctness

- match distribution of the queries
- number of queries witch doesn't match the file they come from by language



# Peformance evaluation

The speed and the memory consumption of the queries will be evaluated when using the tree-sitter engine.

### Queries with the tree sitter engine notes

#### Query parsing time

> To optimize execution : the engine transform the query into a list of capture steps (easier to parcour than a tree), and then eliminate the redondant pattern, and for some queries the number of steps is very high and this steps is exponential (compare 1 by 1, so 2^n). For multi query, the number of steps is high. Additionnaly the number of capture steps is exponential with the depts of the query (making an exponential treatment on an exponential data structure).

#### Query number characters limitation 

Quentin : Number of line of a query limited by u16 (65535) index of pattern, not number of characters.

> solution : load each query individually, then aggregate them. Quentin has implemented a custom matcher.

#### HyperAST implementation

Implement an optimization that accelerate (x10) the matching of query which match rarely (< 500), and slow down if the query match often (> 2000) using precomputation on hyperAST, use the tree sitter implementation of the parser. Reimplemente the parser when an update of tree sitter will breack the interface between hyperAST (rust) and tree sitter (c). Has a custom matcher system.