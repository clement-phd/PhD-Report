# Difference beetwen code tree

## naive approach : use the removed line in the patch

> pros : easy to compute
> cons : can make broken ast, not take in account the addition

## tree diff : use tool to compute the difference between 2 trees

Use tool like gumtree diff (algo ?) to compute the difference between the two tree, take the removed or modified node, and extract the code (to_sexp ?) from the tree


Akira Fujimoto et al., “Staged Tree Matching for Detecting Code Move across Files,” in Proceedings of the 28th International Conference on Program Comprehension (ICPC ’20: 28th International Conference on Program Comprehension, Seoul Republic of Korea: ACM, 2020), 396–400, https://doi.org/10.1145/3387904.3389289.  > project wide ast diff, not suitable, only want deletion/moove without differentiate the 2

### Comparaison
> Goal : use tree diff to define query of tree sitter. In a first step, define a range of code which doesn't brock the AST, in a second, use it to generate tree sitter query more "smart" (more flexible, directly with the difference computed)


- No Chawathe’s algorithm in rust and tree sitter > program one ?  (time consuming, but fun, do exactly what I want), maybe this [tool](https://github.com/afnanenayet/diffsitter) or this [tool](https://github.com/wilfred/difftastic) can help (make the difference in code, but I dont know what exactly it does, it is in rust, output line who change (using ast, not text)). The following paper define good way to represent change, but it is not applicable to see if their are CVE (it define pattern of change to fix something, not how to identify things) : 
Matias Martinez, Laurence Duchien, and Martin Monperrus, “Automatically Extracting Instances of Code Change Patterns with AST Analysis,” in 2013 IEEE International Conference on Software Maintenance (2013 IEEE International Conference on Software Maintenance (ICSM), Eindhoven, Netherlands: IEEE, 2013), 388–91, https://doi.org/10.1109/ICSM.2013.54.
- Gumtree : Java project, interface with rust difficult, the wide advantage is to parse all project, but not really needed, can try to use it.
- Use hyperDiff : Not really feasible now, because it use the hyperAST, maybe use the hyperAst to construct the ast ?


## More flexible tree
- query side : after the generation of the CVE code, and before the generation of the query, remove the commentary for example

- application side : No idea, remove commentary in this side too (in the ast, or in the file) ? (but need to propagate this to the hyperAST, not easy) 