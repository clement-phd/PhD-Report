# Git history

## List the hash of the commit

```shell
git log --pretty=oneline --all
```